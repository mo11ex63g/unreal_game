// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Project_otusGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECT_OTUS_API AProject_otusGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
