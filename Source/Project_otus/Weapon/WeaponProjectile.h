#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;

UCLASS()
class PROJECT_OTUS_API AWeaponProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeaponProjectile();

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(VisibleDefaultsOnly, Category = Projectile)
	USphereComponent* CollisionComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	UProjectileMovementComponent* ProjectileMov;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Projectile, meta=(ClampMin = 0.0f, UIMin = 0.0f))
	float ProjectileInitSpeed = 3000.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Projectile, meta=(ClampMin = 0.0f, UIMin = 0.0f))
	float ProjectileMaxSpeed = 3000.0f;
};
