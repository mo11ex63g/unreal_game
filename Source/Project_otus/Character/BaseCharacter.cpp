#include "BaseCharacter.h"
#include "Camera/CameraComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PawnMovementComponent.h"

ABaseCharacter::ABaseCharacter()
{
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f);
	
	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->TargetArmLength = 400.0f;
	SpringArmComponent->SetupAttachment(RootComponent); 

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);
	CameraComponent->bUsePawnControlRotation = false;
}

void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();	
	if(APlayerController* player_controller = Cast<APlayerController>(Controller))
	{
		if(UEnhancedInputLocalPlayerSubsystem* input_subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(player_controller->GetLocalPlayer()))
		{
			input_subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* input_component)
{
	UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(input_component);
	if (EnhancedInputComponent)
	{
		if(ActionJump)
		{
			EnhancedInputComponent->BindAction(ActionJump, ETriggerEvent::Triggered, this, &ACharacter::Jump);
			EnhancedInputComponent->BindAction(ActionJump, ETriggerEvent::Completed, this, &ACharacter::StopJumping);
		}
		if(ActionMove)
			EnhancedInputComponent->BindAction(ActionMove, ETriggerEvent::Triggered, this, &ABaseCharacter::On_Action_Move);
		if(ActionLook)
			EnhancedInputComponent->BindAction(ActionLook, ETriggerEvent::Triggered, this, &ABaseCharacter::On_Action_Look);
		if(ActionSprint)
		{
			EnhancedInputComponent->BindAction(ActionSprint, ETriggerEvent::Triggered, this, &ABaseCharacter::On_Action_Sprint_Start);
			EnhancedInputComponent->BindAction(ActionSprint, ETriggerEvent::Completed, this, &ABaseCharacter::On_Action_Sprint_End);
		}
	}
	
}

void ABaseCharacter::On_Action_Move(const FInputActionValue& Value)
{
	FVector2d MoveVector = Value.Get<FVector2d>();
	
	if(Controller!=nullptr)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
		
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		
		AddMovementInput(ForwardDirection, MoveVector.X);
		AddMovementInput(RightDirection, MoveVector.Y);
	}
}

void ABaseCharacter::On_Action_Look(const FInputActionValue& Value)
{
	FVector2d LookVector = Value.Get<FVector2d>();

	if(Controller!=nullptr)
	{
		AddControllerYawInput(LookVector.X);
		AddControllerPitchInput(LookVector.Y);
	}
}

void ABaseCharacter::On_Action_Sprint_Start()
{
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
}

void ABaseCharacter::On_Action_Sprint_End()
{
	GetCharacterMovement()->MaxWalkSpeed = RunSpeed;
}

